import Segment from "./Segment";
import HorizontalSegment from "./HorizontalSegment";

class Digit {
  constructor({ ctx, segLength, offsetX, offsetY }) {
    this.ctx = ctx;
    this.segLength = segLength || 120;
    this.offsetX = offsetX || 0;
    this.offsetY = offsetY || 0;

    this.vtl = [0, 4, 5, 6, 8, 9];
    this.vtr = [0, 1, 2, 3, 4, 7, 8, 9];
    this.vbl = [0, 2, 6, 8];
    this.vbr = [0, 1, 3, 4, 5, 6, 7, 8, 9];
    this.ht = [0, 2, 3, 5, 6, 7, 8, 9];
    this.hm = [2, 3, 4, 5, 6, 8, 9];
    this.hb = [0, 2, 3, 5, 6, 8];
  }

  draw(num) {
    this.ctx.push();
    this.ctx.translate(this.offsetX, this.offsetY);

    // h t
    new HorizontalSegment({
      ctx: this.ctx,
      length: this.segLength,
      offsetX: this.segLength / 6,
      glow: num !== "C" ? this.ht.includes(num) : true
    }).draw();

    // vertical top left
    new Segment({
      ctx: this.ctx,
      length: this.segLength,
      offsetY: this.segLength / 6,
      glow: num !== "C" ? this.vtl.includes(num) : true
    }).draw();

    // v bottom left
    new Segment({
      ctx: this.ctx,
      length: this.segLength,
      offsetY: this.segLength + this.segLength / 4,
      glow: num !== "C" ? this.vbl.includes(num) : true
    }).draw();

    // v t r
    new Segment({
      ctx: this.ctx,
      length: this.segLength,
      offsetX: this.segLength + this.segLength / 12,
      offsetY: this.segLength / 6,
      glow: this.vtr.includes(num)
    }).draw();

    // v bottom right
    new Segment({
      ctx: this.ctx,
      length: this.segLength,
      offsetX: this.segLength + this.segLength / 12,
      offsetY: this.segLength + this.segLength / 4,
      glow: this.vbr.includes(num)
    }).draw();

    // h m
    new HorizontalSegment({
      ctx: this.ctx,
      length: this.segLength,
      offsetX: this.segLength / 6,
      offsetY: this.segLength + this.segLength / 12,
      glow: this.hm.includes(num)
    }).draw();

    // h b
    new HorizontalSegment({
      ctx: this.ctx,
      length: this.segLength,
      offsetX: this.segLength / 6,
      offsetY: this.segLength * 2 + this.segLength / 6,
      glow: num !== "C" ? this.hb.includes(num) : true
    }).draw();
    this.ctx.pop();
  }
}

export default Digit;
