import axios from "axios";

// API specific settings https://openweathermap.org/current

class WeatherLocation {
  constructor() {
    this.apiUrl = "https://api.openweathermap.org/data/2.5/weather";
    this.apiKey = process.env.OWA_KEY;
    this.units = "metric";
  }

  async getLocation() {
    if (navigator.geolocation) {
      return new Promise(function(resolve, reject) {
        navigator.geolocation.getCurrentPosition(resolve, () => {
          return Error;
        });
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  async getWeatherData() {
    const position = await this.getLocation();

    if (position) {
      let latitude = position.coords.latitude;
      let longitude = position.coords.longitude;
      let url = `${this.apiUrl}?lat=${latitude}&lon=${longitude}&appid=${
        this.apiKey
      }&units=${this.units}`;

      const data = await axios.get(url);

      return data.data;
    } else {
      console.log("no permission");
      return "No permission";
    }
  }
}

export default WeatherLocation;
