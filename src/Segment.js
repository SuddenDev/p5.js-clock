import Colors from "./Colors";

class Segment {
  constructor({ ctx, offsetX, offsetY, length, glow }) {
    this.ctx = ctx;
    this.offsetX = offsetX || 0;
    this.offsetY = offsetY || 0;
    this.length = length || 120;
    this.glow = glow || false;
  }

  draw() {
    if (this.glow === true) {
      this.ctx.fill(Colors.glow);
    } else {
      this.ctx.fill(Colors.pressed.bg);
    }

    this.ctx.beginShape();

    this.ctx.vertex(this.offsetX + this.length / 8, this.offsetY); // mt
    this.ctx.vertex(this.offsetX, this.offsetY + this.length / 8); // lt
    this.ctx.vertex(this.offsetX, this.offsetY + this.length * 0.875); // lb
    this.ctx.vertex(this.offsetX + this.length / 8, this.offsetY + this.length); // mb
    this.ctx.vertex(
      this.offsetX + this.length / 4,
      this.offsetY + this.length * 0.875
    ); // rb
    this.ctx.vertex(
      this.offsetX + this.length / 4,
      this.offsetY + this.length / 8
    ); //rt

    this.ctx.endShape(this.ctx.CLOSE);
  }
}

export default Segment;
