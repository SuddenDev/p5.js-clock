const colors = {
  bg: "#E9F4FF",
  glow: "#F87575",
  glow2: "FF985F",
  pressed: {
    bg: "#E3EDF7",
    rb: "#fff",
    lt: "rgba(136, 165, 191, 0.48)"
  },
  pushed: {
    bg: "#E9F4FF",
    rb: "rgba(136, 165, 191, 0.48)",
    lt: "#fff"
  }
};

export default colors;
