import Colors from "./Colors";

class Dot {
  constructor({ ctx, offsetX, offsetY, diameter }) {
    this.ctx = ctx;
    this.offsetX = offsetX || 0;
    this.offsetY = offsetY || 0;
    this.diameter = diameter || 16;
  }

  draw(glow) {
    if (glow === true) {
      this.ctx.fill(Colors.glow);
    } else {
      this.ctx.fill(Colors.pressed.bg);
    }

    this.ctx.circle(this.offsetX, this.offsetY, this.diameter);
  }
}

export default Dot;
