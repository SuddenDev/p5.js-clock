import p5 from "p5";

import { getDigit } from "./helpers";
import Digit from "./Digit";
import Colors from "./Colors";
import Dot from "./Dot";
import WeatherLocation from "./WeatherLocation";

const dotenv = require('dotenv').config();

let data;
let wl;

const s = p => {
  const segLength = 80;
  const segLengthSmall = 25;
  const digitMargin = length => {
    return length / 6;
  };
  const digWidth = length => {
    return length * 1.335;
  };

  let clockWidth = digWidth(segLength) * 4 + digitMargin(segLength) * 6;
  let clockHeight = segLength * 2.4;
  let offsetClock = {
    left: p.windowWidth / 2 - clockWidth / 2,
    top: p.windowHeight / 2 - clockHeight / 2
  };

  // digits for clock
  let d1, d2, d3, d4;

  // dots between digits
  let dot1, dot2;
  let dotGlow = true;

  // digits for temperature
  let t0, t1, t2, t3;

  // p5js setup
  p.setup = () => {
    p.createCanvas(p.windowWidth, p.windowHeight);
    p.noStroke();

    d1 = new Digit({
      ctx: p,
      segLength: segLength
    });
    d2 = new Digit({
      ctx: p,
      segLength: segLength,
      offsetX: digWidth(segLength) + digitMargin(segLength)
    });
    d3 = new Digit({
      ctx: p,
      segLength: segLength,
      offsetX: digWidth(segLength) * 2 + digitMargin(segLength) * 5
    });
    d4 = new Digit({
      ctx: p,
      segLength: segLength,
      offsetX: digWidth(segLength) * 3 + digitMargin(segLength) * 6
    });

    // initiallizy dots between digits
    dot1 = new Dot({
      ctx: p,
      offsetX: digWidth(segLength) * 2 + digitMargin(segLength) * 3,
      offsetY: segLength - digitMargin(segLength),
      diameter: segLength / 4
    });

    dot2 = new Dot({
      ctx: p,
      offsetX: digWidth(segLength) * 2 + digitMargin(segLength) * 3,
      offsetY: segLength * 2 - digitMargin(segLength) * 2,
      diameter: segLength / 4
    });

    // temperature digits
    t0 = new Digit({
      ctx: p,
      segLength: segLengthSmall,
      offsetX: 0
    });
    t1 = new Digit({
      ctx: p,
      segLength: segLengthSmall,
      offsetX: digWidth(segLengthSmall) + digitMargin(segLengthSmall) * 2
    });
    t2 = new Digit({
      ctx: p,
      segLength: segLengthSmall,
      offsetX: digWidth(segLengthSmall) * 2 + digitMargin(segLengthSmall) * 4
    });
    t3 = new Digit({
      ctx: p,
      segLength: segLengthSmall,
      offsetX: digWidth(segLengthSmall) * 4
    });
  };

  // p5js draw
  p.draw = () => {
    p.background(Colors.bg);

    p.fill(Colors.glow);
    p.textSize(20);
    p.textAlign(p.CENTER);
    p.text(data.name, offsetClock.left + clockWidth / 2, offsetClock.top - 50);

    p.translate(offsetClock.left, offsetClock.top);

    // Clock
    d1.draw(getDigit(p.hour(), 0));
    d2.draw(getDigit(p.hour(), 1));
    d3.draw(getDigit(p.minute(), 0));
    d4.draw(getDigit(p.minute(), 1));

    if (p.frameCount % 45 === 0) {
      dotGlow = !dotGlow;
    }
    dot1.draw(dotGlow);
    dot2.draw(dotGlow);

    p.translate(clockWidth / 3, clockHeight + 60);

    t0.draw();
    t1.draw(getDigit(data.main.temp, 0));
    t2.draw(getDigit(data.main.temp, 1));
    t3.draw("C");
  };

  p.windowResized = () => {
    p.resizeCanvas(p.windowWidth, p.windowHeight);
    offsetClock.left = p.windowWidth / 2 - clockWidth / 2;
    offsetClock.top = p.windowHeight / 2 - clockHeight / 2;
  };
};

(async () => {
  wl = new WeatherLocation();

  data = await wl.getWeatherData();
  new p5(s, "app");

  if (data) {
    document.querySelector("p").remove();
  }
})();
