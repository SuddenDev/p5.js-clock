import Segment from "./Segment";
import Colors from "./Colors";

class HorizontalSegment extends Segment {
  constructor({ ctx, offsetX, offsetY, length, glow }) {
    super({ ctx, offsetX, offsetY, glow });

    this.length = length || 120;
  }

  draw() {
    this.ctx.beginShape();

    if (this.glow === true) {
      this.ctx.fill(Colors.glow);
    } else {
      this.ctx.fill(Colors.pressed.bg);
    }

    this.ctx.vertex(this.offsetX, this.offsetY + this.length / 8);
    this.ctx.vertex(this.offsetX + this.length / 8, this.offsetY);
    this.ctx.vertex(this.offsetX + this.length * 0.875, this.offsetY);
    this.ctx.vertex(this.offsetX + this.length, this.offsetY + this.length / 8);
    this.ctx.vertex(
      this.offsetX + this.length * 0.875,
      this.offsetY + this.length / 4
    );
    this.ctx.vertex(
      this.offsetX + this.length / 8,
      this.offsetY + this.length / 4
    );

    this.ctx.endShape(this.ctx.CLOSE);
  }
}

export default HorizontalSegment;
