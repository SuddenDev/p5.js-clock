/**
 * Basically sepperating a number into its digits
 * @param {*} num
 * @param {*} index
 */
export function getDigit(num, index) {
  let o = [];
  let sNumber = num.toString();
  if (sNumber.length === 1) {
    sNumber = "0" + sNumber;
  }
  for (var i = 0, len = sNumber.length; i < len; i += 1) {
    o.push(+sNumber.charAt(i));
  }
  return parseInt(o[index], 10);
}
