
# p5.js 7-digit clock

  This project was for a class (Digital Prototyping) at the University of applied sciences Munich.
  It combines a classic 7-digit clock with location and weather data to make it a bit more

![Screenshot of the clock](screenshot_p5clock.png?raw=true "Screenshot")

## To run the code
First install the npm packages with
`npm install`

For running the dev server and parcel
`npm run start`

Building for production
`npm run build`
